from flask import Flask, request
from models import generate_model, read_data
from csv import writer
import pickle
import numpy as np


#Author: Nir Sherf, 2020
#Written for Ofek 324, Data Science Course
#Description: School Solution - MLOPS exercise
#ML model lib - scikit-learn
#Server lib - Flask

app = Flask(__name__)


def load_model(pickle_file_path):
    with open(pickle_file_path, 'rb') as file:
        loaded_model = pickle.load(file)
    return loaded_model


def parse_suspect_from_json(suspect_as_json):
    print(suspect_as_json)
    return np.asarray(list(suspect_as_json.values()), dtype=np.float64)


def add_line_to_csv(arr, path):
    with open(path, 'a+', newline='') as file:
        csv_writer = writer(file)
        csv_writer.writerow(arr)


@app.route('/', methods=['GET', 'POST'])
def homepage():
    return 'ground control to major Tom'


@app.route('/query_example', methods=['GET', 'POST'])
def get_name_from_query():
    if request.method == 'GET':
        name = request.args.get('name')
        if name is not None:
            return 'Hello ' + name
        else:
            return "pass name in query"
    return "pass name Via GET request"


@app.route('/classify_terrorist', methods=['GET', 'POST'])
def classify_terrorist():
    if request.method == 'GET':
        return 'pass suspect properties via Post request'
    elif request.method == 'POST':
        global model
        suspect_properties = parse_suspect_from_json(request.get_json())
        prediction_probability = model.predict_proba([suspect_properties])[0]
        tagged_prediction = list(suspect_properties)
        for field in prediction_probability:
            tagged_prediction.append(field)
        add_line_to_csv(tagged_prediction, 'data/predictions/terrorists_predictions.csv')
        return str(prediction_probability[1]*100)
    return 'hello'


@app.route('/train_model', methods=['GET', 'POST'])
def train_model():
    if request.method == 'GET':
        password = request.args.get('password')
        if password is None:
            return 'send password via query'
        elif password == '1234':
            print("training model")
            generate_model()
            global model
            model = load_model('models/file_example.pkl')
            return 'trained model'
        else:
            return 'password incorrect'
    return 'send password via query'


if __name__ == '__main__':
    read_data()
    global model
    model = load_model('models/file_example.pkl')
    app.run('0.0.0.0', 8000)
