from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
import pickle
import pandas as pd
import datetime
import math

global db1_df, db2_df, db3_df, db4_df, train_df, terrorists_places
# terrorists places with longitude,latitude format:
terrorists_places = [(31.3927, 34.3624),
                    (31.4078, 34.3569),
                    (31.4219, 34.3818),
                    (31.4255, 34.3931),
                    (31.4289, 34.3747),
                    (31.4302, 34.3979),
                    (31.4327, 34.4034),
                    (31.4344, 34.4034),
                    (31.4424, 34.3749),
                    (31.4445, 34.4011)]

def read_data():
    global db1_df, db2_df, db3_df, db4_df, train_df, terrorists_places
    path = "C://Work/Army/Data Science for developers/קורס מיצוי מידע/תרגיל סיום - MLOPS/data-science-course-mlops-school-solution/data/train_data/"
    db1_df = pd.read_csv(path + "db1.csv")
    db2_df = pd.read_csv(path + "db2.csv")
    db3_df = pd.read_csv(path + "db3.csv")
    db4_df = pd.read_csv(path + "db4.csv")
    train_df = pd.read_csv(path + "train_data.csv")
    db1_df.index = db1_df.id
    db2_df.index = db2_df.arrested_id
    db3_df.index = db3_df.suspect_id

def compute_ages_of_suspects(suspects_ids):
    """
    input:
        suspects_ids - list of suspects id to compute ages for
    return:
        list of ages matching the given suspects_ids list
    """
    global db1_df
    birth_dates_strs = list(db1_df.loc[suspects_ids, "birth_date"])
    # we extract the format of the birth date by inspecting the first rows of the db1 dataFrame
    birth_dates_datetimes = [datetime.datetime.strptime(birth_date_str, "%Y-%m-%d %H:%M:%S.%f") for
                             birth_date_str in birth_dates_strs]
    time_now = datetime.datetime.now()
    ages = [round((time_now - birth_date).days/365, 1) for birth_date in birth_dates_datetimes]
    return ages

def is_father_terrorist(suspects_ids):
    """
    input: list of suspects ids
    output: list of booleans indicating for each suspect id if his father is a terrorist (approximated by the arrests data)
    """
    global db1_df, db2_df
    fathers_ids = list(db1_df.loc[suspects_ids, "father_id"])
    arrested_ids = set(db2_df.index)
    is_father_terrorist = [father_id in arrested_ids for father_id in fathers_ids]
    return is_father_terrorist

def create_father_to_oldest_son_dict():
    """
    this function will return a dictionary that maps for each father_id of father in gaza, the (id, birth_date) of it's oldest
    son.
    """
    global db1_df
    father_to_oldest_son_dict = {}
    for father_id, son_id, son_birth_date_str in db1_df.loc[:, ['father_id', 'id', 'birth_date']].values:
        son_birth_date = datetime.datetime.strptime(son_birth_date_str, "%Y-%m-%d %H:%M:%S.%f")
        if father_id not in father_to_oldest_son_dict:
            father_to_oldest_son_dict[father_id] = (son_id, son_birth_date)
        else:
            if(father_to_oldest_son_dict[father_id][1] > son_birth_date): # we got older son of the father:
                father_to_oldest_son_dict[father_id] = (son_id, son_birth_date)
    return father_to_oldest_son_dict

def get_oldests_brother_of_suspects(suspects_ids):
    """
    input: list of suspects_ids
    output:
        list that contain the oldest brother for each suspect id. if the suspect is the oldest brother in the family
        so it will return his id.
    """
    global db1_df
    fathers_ids = list(db1_df.loc[suspects_ids, "father_id"])
    father_to_oldest_son_dict = create_father_to_oldest_son_dict()
    oldest_brother = [father_to_oldest_son_dict[father_id][0] for father_id in fathers_ids]
    return oldest_brother

def is_suspect_oldest_brother(suspects_ids):
    """
    input:
        suspects_ids - list of suspects_ids

    """
    suspects_oldest_brothers = get_oldests_brother_of_suspects(suspects_ids)
    is_oldest_brother = [suspects_ids[i]==suspects_oldest_brothers[i] for i in range(len(suspects_ids))]
    return is_oldest_brother

def is_oldest_brother_terrorist(suspects_ids):
    """
    input:
        suspects_ids - list of suspects_ids
    output:
        a list of booleans indicating if the oldest brother of the suspect was once arrests as a terrorist (according
        to db2_df)
    """
    global db2_df
    suspects_oldest_brothers = get_oldests_brother_of_suspects(suspects_ids)
    arrested_ids = set(db2_df.index)
    return [oldest_brother_id in arrested_ids for oldest_brother_id in suspects_oldest_brothers]

def is_suspect_a_parent(suspects_ids):
    """
    input:
        suspects_ids - list of suspects ids
    output:
        list of boolean that indicates if the suspect is a parent (a father or a mother of someone in gaza)
    """
    # first, let's extract id's of father and mothers is gaza:
    global db1_df
    parents_ids = set(db1_df["father_id"]).union(set(db1_df["mom_id"]))
    is_parent = [suspect_id in parents_ids for suspect_id in suspects_ids]
    return is_parent

def get_suspects_salaries(suspects_ids):
    """
    input:
        list of suspects ids
    output:
        list of annual salaries of the given suspects_ids
    """
    global db3_df
    id_to_salary_dict = {}
    for sus_id, salary in db3_df[["suspect_id", "annual_salary"]].values:
        id_to_salary_dict[sus_id] = salary
    salaries = [id_to_salary_dict[suspect_id] if suspect_id in id_to_salary_dict else 0 for suspect_id in suspects_ids]
    return salaries


def were_suspsects_arrested(suspects_ids):
    """
    input:
        suspects_ids - list of ids of suspects
    output:
        list of booleans indicating if the suspects in the given list were previously arrested as terrorist before
    """
    global db2_df
    arrested_ids = set(db2_df.index)
    return [suspect_id in arrested_ids for suspect_id in suspects_ids]


def create_total_sons_per_father_dict():
    """
    this function will return a dictionary that maps for each father_id of father in gaza, the total number of his sons
    """
    global db1_df
    father_to_total_sons_dict = {}
    for father_id in list(db1_df['father_id']):
        if father_id not in father_to_total_sons_dict:
            father_to_total_sons_dict[father_id] = 1
        else:
            father_to_total_sons_dict[father_id] += 1
    return father_to_total_sons_dict

def is_only_son(suspects_ids):
    """
    input:
        suspects_ids - list of suspsects ids
    output:
        list of boolean that indicates for each suspect if he is the only son of his father
    """
    global db1_df
    father_to_total_sons_dict = create_total_sons_per_father_dict()
    fathers_ids = list(db1_df.loc[suspects_ids, "father_id"])
    is_only_son = [father_to_total_sons_dict[father_id] == 1 for father_id in fathers_ids]
    return is_only_son


def get_total_calls_near_terrorists_places(suspects_ids):
    """
    input:
        suspects_ids- list of suspects_ids
    output:
        3 lists: total_calls_100_meter_from_terrorists_place, total_calls_500_meter_from_terrorists_place,
                                                            total_calls_1000_meter_from_terrorists_place
        each list represents for each suspect the total number of calls he had near a terrorist place, with the
                matched notion of "near"
    """
    total_calls_100_meter_from_terrorists_place = []
    total_calls_500_meter_from_terrorists_place = []
    total_calls_1000_meter_from_terrorists_place = []
    for suspect_id in suspects_ids:
        suspect_calls_df = load_suspect_calls(suspect_id)
        total_calls_100_meter_from_terrorists_place.append(get_total_calls_near_places(suspect_calls_df,
                                                                                       terrorists_places, 0.1))
        total_calls_500_meter_from_terrorists_place.append(get_total_calls_near_places(suspect_calls_df,
                                                                                       terrorists_places, 0.5))
        total_calls_1000_meter_from_terrorists_place.append(get_total_calls_near_places(suspect_calls_df,
                                                                                        terrorists_places, 1))
    return total_calls_100_meter_from_terrorists_place, total_calls_500_meter_from_terrorists_place, \
           total_calls_1000_meter_from_terrorists_place


def load_suspect_calls(suspect_id, db5_path="C://Work/Army/Data Science for developers/קורס מיצוי מידע/תרגיל סיום - MLOPS/data-science-course-mlops-school-solution/data/train_data/db5"):
    """
    input:
        suspect_id - id of a suspect
    output:
        the dataFrame that represents the calls the suspect had.
    """
    file_path = db5_path + "/" + str(suspect_id) + "_calls.csv"
    calls_df = pd.read_csv(file_path)
    return calls_df


def get_total_calls_near_places(calls_df, places, near_dist):
    """
    input:
        calls_df - pandas dataFrame that represents list of calls. the columns of the data frame contains the columns:
                    call_location_longitude, call_location_latitude
        places - list of the format: [(longitude_1, latitude_1),...., (longitude_n, latitude_n)] that represents locations
                    of places
        near_dist - ditance in KM (floating number)
    output:
        the function will return
    """
    total_calls_near_places = 0
    for call_longitude, call_latitude in calls_df[["call_location_longitude", "call_location_latitude"]].values:
        closest_dist_to_places = get_geodetic_distance_to_closest_point(call_longitude, call_latitude, places)
        if (closest_dist_to_places <= near_dist):
            total_calls_near_places += 1
    return total_calls_near_places


def get_geodetic_distance_to_closest_point(point_longitude, point_latitude, places):
    """
    input:
        point_longitude - longitude of a point
        point_latitude - latitude of a point
        places - list of places of the form: [(longitude_1, latitude_1), ...., (longitude_n, latitude_n)]
    output:
        the geodetic distance (in km) between the given point and the closest point to in from places
    """
    return min([geodetic_distance((point_longitude, point_latitude), place) for place in places])


def geodetic_distance(p1, p2):
    """
    p1 and p2 are points of the form (long, lat)
    the function will return the distance in km between them.
    this in the implementation we gave to you in the helper file mentioned in the ex
    """
    earth_radius = 6371
    phi1 = p1[1] / 180 * math.pi
    phi2 = p2[1] / 180 * math.pi
    lambda1 = p1[0] / 180 * math.pi
    lambda2 = p2[0] / 180 * math.pi
    delta_phi = phi1 - phi2
    delta_lambda = lambda1 - lambda2
    a = math.sin(delta_phi / 2) * math.sin(delta_phi / 2) + \
        math.cos(phi1) * math.cos(phi2) * \
        math.sin(delta_lambda / 2) * math.sin(delta_lambda / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    return earth_radius * c

def get_total_short_calls_to_terrorists(suspects_ids):
    """
    input:
        suspects_ids- list of suspects_ids
    output:
        3 lists: total_short_calls_30, total_short_calls_60, total_short_calls_300
        each list represents for each suspect the total number of calls he had to a terrorist which are shorter than 30, 60
        and 300 seconds respectively
    """
    global db2_df
    total_short_calls_30 = []
    total_short_calls_60 = []
    total_short_calls_300 = []
    terrorists_ids = set(db2_df.index)
    for suspect_id in suspects_ids:
        suspect_calls_df = load_suspect_calls(suspect_id)
        total_short_calls_30.append(count_total_short_calls_to_terrorists(suspect_calls_df, 30, terrorists_ids))
        total_short_calls_60.append(count_total_short_calls_to_terrorists(suspect_calls_df, 60, terrorists_ids))
        total_short_calls_300.append(count_total_short_calls_to_terrorists(suspect_calls_df, 300, terrorists_ids))
    return total_short_calls_30, total_short_calls_60, total_short_calls_300

def count_total_short_calls_to_terrorists(calls_df, short_threshold, terrorists_ids):
    """
    input:
        calls_df - pandas dataFrame that contains information about phone calls. it contains the columns:
                    phone_number, call_duration
        short_threshold - int number that represents the length of a call in seconds, that shorter calls from it will be
                        considered short, and longer calls from it will be considered long
        terrorists_ids - set that contains all the ids of terrorists as specified in db2
    output:
        the function will return the number of shorts that are shorter than short_thresholds that were executed to
        a terrorist that is found in the terrorists_ids list.
    """
    global db4_df
    phone_to_id_dict = {}
    for phone, sus_id in db4_df[["phone", "id"]].values:
        phone_to_id_dict[phone] = sus_id
    total_calls = 0
    for other_side_phone_number, call_duration in calls_df[["phone_number", "call_duration"]].values:
        if((call_duration < short_threshold) and (phone_to_id_dict[other_side_phone_number] in terrorists_ids)):
            total_calls += 1
    return total_calls


def compute_features_df(suspects_ids):
    """
    input:
        suspects_ids - list of suspects ids
    output:
        pandas dataFrame that holds all the features for the given suspects_id. those features represents the business
        information.
    """
    #total_calls_100_meter_from_terror, total_calls_500_meter_from_terror, total_calls_1000_meter_from_terror = \
    #    get_total_calls_near_terrorists_places(suspects_ids)

    #total_short_calls_30, total_short_calls_60, total_short_calls_300 = get_total_short_calls_to_terrorists(
    #    suspects_ids)

    features_df = pd.DataFrame({
        'age': compute_ages_of_suspects(suspects_ids),
        'is_father_terrorist': is_father_terrorist(suspects_ids),
        'is_oldest_brother': is_suspect_oldest_brother(suspects_ids),
        'is_oldest_brother_terrorist': is_oldest_brother_terrorist(suspects_ids),
        'is_parent': is_suspect_a_parent(suspects_ids),
        'salary': get_suspects_salaries(suspects_ids),
        'was_arrested': were_suspsects_arrested(suspects_ids),
        'is_only_son': is_only_son(suspects_ids)
        #'total_calls_100_meter_from_terrorists_place': total_calls_100_meter_from_terror,
        #'total_calls_500_meter_from_terrorists_place': total_calls_500_meter_from_terror,
        #'total_calls_1000_meter_from_terrorists_place': total_calls_1000_meter_from_terror,
        #'total_calls_to_terrorist_shorter_than_30_seconds': total_short_calls_30,
        #'total_calls_to_terrorist_shorter_than_60_seconds': total_short_calls_60,
        #'total_calls_to_terrorist_shorter_than_300_seconds': total_short_calls_300
    })
    return features_df

def generate_model():
    global train_df
    train_X = compute_features_df(list(train_df["suspect_id"]))
    train_Y = train_df.is_terrorist
    core_train_X, validation_X, core_train_Y, validation_Y = train_test_split(train_X, train_Y, test_size=0.3)
    model = GradientBoostingClassifier(n_estimators=500, learning_rate=0.006).fit(core_train_X, core_train_Y)

    with open('models/file_example.pkl', 'wb') as file:
        pickle.dump(model, file)
